import os
from docx import Document
import pandas as pd


def update_text(obj, replacements):
    for para in obj.paragraphs:
        for key, value in replacements.items():
            if f"<<{key}>>" in para.text:
                para.text = para.text.replace(f"<<{key}>>", str(value))

    if hasattr(obj, 'tables'):  # Check if the object has tables attribute (Document and _Cell have tables)
        for table in obj.tables:
            for row in table.rows:
                for cell in row.cells:
                    update_text(cell, replacements)  # Recursively update cell text


def update_header_footer(doc, replacements):
    for section in doc.sections:
        # Update default header and footer
        update_text(section.header, replacements)
        update_text(section.footer, replacements)

        # Update first page header and footer, if they exist
        if section.first_page_header:
            update_text(section.first_page_header, replacements)
        if section.first_page_footer:
            update_text(section.first_page_footer, replacements)

        # Update even page header and footer, if they exist
        if section.even_page_header:
            update_text(section.even_page_header, replacements)
        if section.even_page_footer:
            update_text(section.even_page_footer, replacements)


def update_document(doc_path, replacements, output_dir):
    doc = Document(doc_path)

    # Update text in paragraphs
    update_text(doc, replacements)

    # Update text in headers and footers
    update_header_footer(doc, replacements)

    # Save the updated document in the output directory
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)
    base_name = os.path.basename(doc_path)
    # Getting the original file name back.
    filename = base_name.replace("_Template", "")
    output_path = os.path.join(output_dir, filename)
    doc.save(output_path)


def validate_excel_file(excel_file):
    if not os.path.isfile(excel_file):
        raise FileNotFoundError(f"The Excel file '{excel_file}' does not exist.")


def validate_template_directory(template_directory):
    if not os.path.isdir(template_directory):
        raise NotADirectoryError(f"The template directory '{template_directory}' does not exist.")

    if not os.listdir(template_directory):
        raise FileNotFoundError(f"The template directory '{template_directory}' is empty.")


def main():
    default_excel_file = os.path.join(os.getcwd(), "UDData.xlsx")

    # Get user input for the Excel file
    excel_file = input(
        f"Enter the full path of the Excel file (default: '{default_excel_file}'): ") or default_excel_file

    # Validate the Excel file
    try:
        validate_excel_file(excel_file)
    except FileNotFoundError as e:
        print(e)
        return

    # Get user input for the template directory
    default_template_directory = os.path.join(os.getcwd(), "Templates")
    template_directory = input(
        f"Enter the full path of the template directory (default: {default_template_directory}): ") or default_template_directory

    # Validate the template directory
    try:
        validate_template_directory(template_directory)
    except (FileNotFoundError, NotADirectoryError) as e:
        print(e)
        return

    # Get user input for the output directory
    default_output_directory = os.path.join(template_directory, 'updated_document')
    output_directory = input(
        f"Enter the full path of the output directory (default: {default_output_directory}): ") or default_output_directory

    # Create the output directory if it doesn't exist
    os.makedirs(output_directory, exist_ok=True)

    # Read the Excel file
    df = pd.read_excel(excel_file)

    # Convert all datetime columns to dates (removing time)
    for col in df.select_dtypes(include=['datetime64[ns]']).columns:
        df[col] = df[col].dt.strftime('%d/%b/%Y')

    for index, row in df.iterrows():
        # First column contains the file name
        filename = row.iloc[0]
        # Split the filename and extension
        name, extension = os.path.splitext(filename)

        # Second column contains the custom name to be added to the output file name
        custom_name = row.iloc[1]

        # Add '_Template' before the extension and reconstruct the filename
        doc_filename = f"{name}_Template{extension}"

        # Path to the template document
        doc_path = os.path.join(template_directory, doc_filename)

        # Check if the file exists
        if not os.path.exists(doc_path):
            print(f"############ Template {doc_filename} DOES NOT exist, Continue to Next... ###########")
            continue  # Skip to the next iteration

        print(doc_path)

        # Ensure the path is a Word document and exists
        if doc_path.endswith('.docx') and os.path.exists(doc_path):
            # The rest of the columns are fieldname-value pairs
            replacements = row.iloc[1:].to_dict()

            # Update the document based on the replacements
            update_document(doc_path, replacements, output_directory)

    # Prompt user to press enter before exiting
    input("Press Enter to exit...")


if __name__ == "__main__":
    main()
