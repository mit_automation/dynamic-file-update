# Dynamic File Update

## Update UD and other templates
This project provides a tags based mechanism to dynamically update the documents based upon custom defined Templates.

## Description
The user can define the file name and the corresponding template description in the UDData.xls file. The template should have have the corresponding tags as described in the xls file. Some are predefefined tags, but they can be updated or even new tags can be added.

## Installation
Requires Python and the python packages "pandas" and "docx" must be installed.

## Usage
1) Update UDData.xls file.
2) Create the corresponding template files in Templates folder. Some samples are already there. Be careful with the tag names, format, spacing etc. 
3) Run the Executable file. This will ask for the location of Excel file and the Template Directory. Default location is current director and the Templates folder inside that. 
4) For any behaviour changes, update the python file. 

## Authors
Dr. Samar Shailendra

## License
This is distributed under GNU GPL v3.0

## Project status
This is always under development with new features and templates.
